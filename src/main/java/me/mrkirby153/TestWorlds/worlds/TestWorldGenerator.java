package me.mrkirby153.TestWorlds.worlds;

import cpw.mods.fml.common.IWorldGenerator;
import me.mrkirby153.TestWorlds.handlers.LogHandler;
import net.minecraft.init.Blocks;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import org.apache.logging.log4j.Level;

import java.util.Random;

public class TestWorldGenerator implements IWorldGenerator {
    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
        TestWorld worldInfo = WorldHandler.fromId(world.provider.dimensionId);
        if (worldInfo == null) {
            LogHandler.log(Level.DEBUG, "Dimension " + world.provider.dimensionId + " is not a registered TestWorld!");
            return;
        }
        generateSpawnPlatform(world);
    }

    private void generateSpawnPlatform(World w){
        int midX = 8;
        int midZ = 8;
       // Plus x + z
        int bounds = 4;
        for(int x = -bounds; x <= bounds; x++){
            for(int  z = -bounds; z <= bounds; z++){
                w.setBlock(midX + bounds, w.getSpawnPoint().posY, midZ + bounds, Blocks.stone);
            }
        }
    }
}
