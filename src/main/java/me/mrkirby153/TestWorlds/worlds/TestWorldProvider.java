package me.mrkirby153.TestWorlds.worlds;

import me.mrkirby153.TestWorlds.handlers.LogHandler;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.ChunkProviderFlat;
import net.minecraft.world.gen.ChunkProviderGenerate;
import org.apache.logging.log4j.Level;

public class TestWorldProvider extends WorldProvider {


    private TestWorld worldInfo;
    private static final String SUPERFLAT_DEFAULT_ARGUMENTS = "2;7,2x3,2;1;village";

    @Override
    public long getSeed() {
        getWorldInfo();
        return worldInfo.getSeed();
    }

    @Override
    public boolean canRespawnHere() {
        getWorldInfo();
        return worldInfo.canRespawn();
    }

    @Override
    public IChunkProvider createChunkGenerator() {
        LogHandler.log(Level.DEBUG, "Creating chunk generator for dimension " + dimensionId);
        getWorldInfo();
        switch (worldInfo.worldType) {
            case NORMAL:
                return new ChunkProviderGenerate(worldObj, getSeed(), true);
            case SUPERFLAT:
                return new ChunkProviderFlat(worldObj, getSeed(), true, SUPERFLAT_DEFAULT_ARGUMENTS);
            default:
                return new ChunkProviderGenerate(worldObj, getSeed(), true);
        }
    }

    private TestWorld getWorldInfo() {
        if (worldInfo != null)
            return worldInfo;
        LogHandler.log(Level.DEBUG, "Loading world info for dimension " + dimensionId);
        TestWorld info = WorldHandler.fromId(dimensionId);
        if (info == null) {
            LogHandler.log(Level.ERROR, "Dimension information for " + dimensionId + " is missing!");
        }
        this.worldInfo = info;
        return info;
    }

    @Override
    public String getDimensionName() {
        return "testworld";
    }
}
