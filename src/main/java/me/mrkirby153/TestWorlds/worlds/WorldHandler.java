package me.mrkirby153.TestWorlds.worlds;

import com.google.gson.Gson;
import me.mrkirby153.TestWorlds.handlers.LogHandler;
import net.minecraftforge.common.DimensionManager;
import org.apache.logging.log4j.Level;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class WorldHandler {


    private static HashMap<UUID, TestWorld> loadedWorlds = new HashMap<>();
    private static HashMap<Integer, TestWorld> worldIndex = new HashMap<>();
    private static ArrayList<Integer> reclaimedIds = new ArrayList<>();

    public static void initialize() {
        // Get a list of reclaimed ids
        File reclaimedIds = getDataDir("reclaimed_ids");
        try{
            LogHandler.log(Level.DEBUG, "Attempting to reclaim dimension ids");
            BufferedReader reader = new BufferedReader(new FileReader(reclaimedIds));
            String idsRaw = reader.readLine();
            String[] ids = idsRaw.split(",");
            for(String s : ids){
                WorldHandler.reclaimedIds.add(Integer.valueOf(s));
            }
        } catch (Exception e){
            LogHandler.logError(e);
        }
        // Load from file
        Gson gson = new Gson();
        File saveDataDir = getDataDir("saves");
        File[] fileList = saveDataDir.listFiles();
        if(fileList == null) {
            LogHandler.log(Level.INFO, "Skipping loading of dimensions");
            LogHandler.log(Level.DEBUG, "No files found in directory "+saveDataDir.getAbsolutePath());
            return;
        }
        try {
            for (File f : fileList) {
                BufferedReader reader = new BufferedReader(new FileReader(f));
                String rawJson = reader.readLine();
                TestWorld world = gson.fromJson(rawJson, TestWorld.class);
                loadedWorlds.put(world.getOwner(), world);
                worldIndex.put(world.getDimensionId(), world);
            }
        } catch (Exception e){
            LogHandler.logError(e);
        }
    }

    public static void saveWorlds() {
        File saveDataDir = getDataDir("saves");
        try {
            for (TestWorld w : loadedWorlds.values()) {
                Gson g = new Gson();
                String gsonSerialized = g.toJson(w);
                LogHandler.log(Level.DEBUG, "Writing: " + gsonSerialized);
                FileWriter writer = new FileWriter(new File(saveDataDir, Integer.toString(w.getDimensionId())));
                writer.write(gsonSerialized.toCharArray());
                writer.close();
            }
        } catch (Exception e) {
            LogHandler.logError(e);
        }
    }

    public static void constructWorld(TestWorld world){

    }

    public static TestWorld fromId(int dimensionId){
        return worldIndex.get(dimensionId);
    }

    private static File getDataDir(){
        File saveDataDir = new File(DimensionManager.getCurrentSaveRootDirectory(), File.separator + "testWorldData");
        if (!saveDataDir.exists())
            saveDataDir.mkdirs();
        return saveDataDir;
    }

    private static File getDataDir(String name){
        File dataDir = new File(getDataDir(), name);
        if(!dataDir.exists())
            dataDir.mkdirs();
        return dataDir;
    }
}
