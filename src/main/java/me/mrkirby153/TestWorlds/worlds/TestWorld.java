package me.mrkirby153.TestWorlds.worlds;

import net.minecraft.entity.player.EntityPlayer;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class TestWorld {

    private UUID owner;

    private int dimensionId = -1;

    private ArrayList<UUID> allowedVisitors;

    private long seed;

    public WorldType worldType;

    private final boolean canRespawn = true;


    public TestWorld(UUID owner, int dimensionId){
        this.owner = owner;
        this.dimensionId = dimensionId;
    }

    public TestWorld(UUID owner){
        this.owner = owner;
    }

    public UUID getOwner() {
        return owner;
    }

    public int getDimensionId() {
        return dimensionId;
    }

    public ArrayList<UUID> getAllowedVisitors() {
        return allowedVisitors;
    }

    public boolean canJoinWorld(UUID uuid){
        return allowedVisitors.contains(uuid);
    }

    public boolean canJoinWorld(EntityPlayer entityPlayer){
        return allowedVisitors.contains(entityPlayer.getUniqueID());
    }

    private long generateString(){
        Random r = new Random();
        this.seed = r.nextLong();
        return this.seed;
    }

    public long getSeed() {
        return seed;
    }

    public WorldType getWorldType() {
        return worldType;
    }

    public boolean canRespawn() {
        return canRespawn;
    }

    public enum WorldType{
        SUPERFLAT,
        NORMAL,
    }
}
