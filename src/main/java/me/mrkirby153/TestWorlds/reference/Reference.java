package me.mrkirby153.TestWorlds.reference;

public class Reference {

    public static final String MOD_ID = "TestWorlds";
    public static final String MOD_NAME = "Test Worlds";
    public static final String MOD_VERSION = "@VERSION@";

    public static final String SERVER_PROXY_CLASS = "me.mrkirby153.TestWorlds.proxy.ClientProxy";
    public static final String CLIENT_PROXY_CLASS = "me.mrkirby153.TestWorlds.proxy.CommonProxy";
}
