package me.mrkirby153.TestWorlds.reference;

public class Settings {


    public static class Logging {
        public static boolean DEBUG_ENABLED = false;
        public static boolean FILE_LOGGING = false;
    }

    public static class Dimensions {
        public static int STARTING_ID = -1500;
        public static boolean INCREASE = false;
    }
}
