package me.mrkirby153.TestWorlds;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import cpw.mods.fml.relauncher.Side;
import me.mrkirby153.TestWorlds.command.CommandTestWorld;
import me.mrkirby153.TestWorlds.handlers.ConfigurationHandler;
import me.mrkirby153.TestWorlds.handlers.LogHandler;
import me.mrkirby153.TestWorlds.proxy.CommonProxy;
import me.mrkirby153.TestWorlds.reference.Reference;
import me.mrkirby153.TestWorlds.worlds.WorldHandler;

import java.io.File;

@Mod(modid = Reference.MOD_ID, version = Reference.MOD_VERSION, name = Reference.MOD_NAME)
public class TestWorlds {

    @Mod.Instance(Reference.MOD_ID)
    public static TestWorlds instance;

    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
    public static CommonProxy proxy;


    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.preInit();
        ConfigurationHandler.init(new File(event.getModConfigurationDirectory(), Reference.MOD_ID + "/config.cfg"));
        if (event.getSide() == Side.SERVER)
            LogHandler.init(new File(event.getModConfigurationDirectory(), Reference.MOD_ID + "/log-server.txt"));
        else
            LogHandler.init(new File(event.getModConfigurationDirectory(), Reference.MOD_ID + "/log-client.txt"));
    }


    @Mod.EventHandler
    public void serverStarting(FMLServerStartingEvent event) {
        proxy.serverStarting();
        event.registerServerCommand(new CommandTestWorld());
        WorldHandler.initialize();
    }

    @Mod.EventHandler
    public void serverStopping(FMLServerStoppingEvent event) {
        proxy.serverStopping();
        WorldHandler.saveWorlds();
    }
}
