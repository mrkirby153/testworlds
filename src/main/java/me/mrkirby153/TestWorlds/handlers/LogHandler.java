package me.mrkirby153.TestWorlds.handlers;

import me.mrkirby153.TestWorlds.reference.Reference;
import me.mrkirby153.TestWorlds.reference.Settings;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;

public class LogHandler {

    private static Logger logger;
    private static File logFile;
    private static boolean fileLoggingEnabled;

    public static void init(File logFile) {
        logger = LogManager.getLogger(Reference.MOD_ID);
        if(Settings.Logging.FILE_LOGGING) {
            LogHandler.logFile = logFile;
            fileLoggingEnabled = true;
            log(Level.INFO, "Logging to "+logFile.getAbsolutePath(), false);
        }
    }

    /**
     * Logs a message to the console and to the logfile if enabled
     *
     * @param logLevel The level to log at
     * @param message  The message to log
     */
    public static void log(Level logLevel, String message) {
        if (logLevel == Level.DEBUG) {
            if (Settings.Logging.DEBUG_ENABLED)
                logger.log(Level.INFO, "[DEBUG] " + message);
        } else {
            logger.log(logLevel, message);
        }
        String logfileMessage = "[" + logLevel.toString() + "] " + message;
        if (fileLoggingEnabled)
            logToFile(logfileMessage);
    }

    /**
     * Logs a message to the console and the logfile if enabled
     *
     * @param logLevel      The level to log at
     * @param message       The message to log
     * @param bypassLogFile Determines if the logfile should be bypassed
     */
    public static void log(Level logLevel, String message, boolean bypassLogFile) {
        if (!bypassLogFile)
            log(logLevel, message);
        else
            logger.log(logLevel, message);
    }

    /**
     * Logs to the logfile.
     *
     * @param message The message to log
     */
    public static void logToFile(String message) {
        SimpleDateFormat df = new SimpleDateFormat("YYY-MM-dd hh:mm:ss");
        String dateStamp = df.format(System.currentTimeMillis()) + " ";
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.write(dateStamp + message + "\n");
            writer.close();
        } catch (IOException e) {
//            log(Level.FATAL, "There was an error writing to the log file!", false);
            logger.catching(Level.FATAL, e);
        }
    }

    public static void logError(Throwable t) {
        logger.catching(Level.FATAL, t);
    }
}
