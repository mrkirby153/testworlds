package me.mrkirby153.TestWorlds.handlers;

import me.mrkirby153.TestWorlds.reference.Settings;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

import java.io.File;

public class ConfigurationHandler {

    public static Configuration configuration;

    public static void init(File configurationFile){
       if(configuration == null){
           configuration = new Configuration(configurationFile, true);
           loadConfig();
       }
    }

    private static void loadConfig(){
        Property debugProperty = configuration.get("logging", "debugEnabled", false);
        debugProperty.comment = "Enables debug logging to the console. (Default: false)";
        Settings.Logging.DEBUG_ENABLED = debugProperty.getBoolean();
        Property logToFile = configuration.get("logging", "logToFile", false);
        logToFile.comment = "Enables logging to a file. (Default: false)";
        Settings.Logging.FILE_LOGGING = logToFile.getBoolean();

        Property dimensionId = configuration.get("dimensions", "startingId", -1500);
        dimensionId.comment = "The starting dimension ID for test worlds (Default: -1500)";
        Settings.Dimensions.STARTING_ID = dimensionId.getInt();
        Property increaseDecrease = configuration.get("dimensions", "increase", false);
        increaseDecrease.comment = "Increase or decrease dimensions (True: increase, false: decrease) (Default: false)";
        Settings.Dimensions.INCREASE = increaseDecrease.getBoolean();

        if(configuration.hasChanged())
            configuration.save();
    }
}
